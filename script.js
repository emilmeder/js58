'use strict';

let post = {
  id: 1,
  imageURL:"https://66.media.tumblr.com/8d5ba85937ade07e715394193e3131a0/tumblr_p4kn7tMbDY1vj1alpo6_500.jpg",
};

let comment = {
  user: "user1",
  id: 1,
};

let newPost = createPostElement(post);

let newComment = createCommentElement(comment);

function createPostElement(post) {
	let elemental = document.createElement(`div`);
  elemental.innerHTML = 
      `<div>
        <img class="d-block w-100" src="${post.imageURL}" alt="Post image">
      </div>
      <div class="px-4 py-3">
        <!-- post reactions block start -->
        <div class="d-flex justify-content-around">
				<span class="h1 mx-2 text-danger">
				  <i class="fas fa-heart" id = "like"></i>
				</span>
          <span class="h1 mx-2 muted">
            <i class="far fa-comment"></i>
          </span>
          <span class="mx-auto"></span>
          <span class="h1 mx-2 muted">
            <i class="fas fa-bookmark"></i>
          </span>
        </div>
        <hr>
        <div>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ad est cumque nulla voluptatem enim voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>
          </div>
        <hr>
        <div>
          <div class="py-2 pl-3">
            <a href="#" class="muted">someusername</a>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ad est cumque nulla voluptatem enim voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>
          </div>
          <div class="py-2 pl-3">
            <a href="#" class="muted">someusername</a>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ad est cumque nulla voluptatem enim voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>
          </div>
        </div>
      </div>`;
      
	let newAttribute = document.createAttribute("class");
  newAttribute.value = "posts-container";
  
	elemental.setAttributeNode(newAttribute);
	return elemental;
}

function createCommentElement(comment) {
  let elemental = document.createElement(`div`);
  elemental.innerHTML = "<a href=# class=muted>" + comment.user + "</a>";
  elemental.innerHTML = `<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Chooh chooh mot$erF*c#er Ipsum ad est cumque nulla voluptatem enim voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>`;
  
	let newAttribute = document.createAttribute("class");
  newAttribute.value = "py-2 pl-3";
  
	elemental.setAttributeNode(newAttribute);
	return elemental;
}

function  showSplashScreen(){
  let one = document.getElementById("page-splash");
  // let two = document.getElementById('body');

  one.hidden = false;
  // two.body.classList.add("no-scroll");
  document.body.classList.add("no-scroll");
}


function  hideSplashScreen(){
  let one = document.getElementById("page-splash");
  // let two = document.getElementById('body');

  one.hidden = true;
  // two.body.classList.remove("no-scroll");
  	document.body.classList.remove("no-scroll");
}

// function showSplashScreen() {


let postElement = createPostElement(post);

let commentElement = createCommentElement(comment);

function addPost(elemental){
	document.getElementsByClassName('col col-lg-7 posts-container')[0].append(elemental);
}


function addComments(elemental){
	document.getElementsByClassName('py-2 pl-3')[1].append(elemental);
}

let like = document.getElementById('like');
  like.addEventListener('click', function(){
    let heart = like.children[0];
  if(heart.classList.contains("text-danger")){
    
      heart.classList.remove("text-danger","fas");
  }
  else{
    heart.classList.add("text-danger","fas");
  }
});
// let book = document.getElementById('book');
//   like.addEventListener('click', function(){
//     let add = like.children[0];
//   if(add.classList.contains("far")){
//     add.classList.remove("far");
//   }
//   else{
//     add.classList.add("far");
//   }
// });

let book = document.getElementsByClassName("fas fa-bookmark")[0];
book.addEventListener("click", function(){
  
  this.style.color = (this.style.color == 'gray') ? 'black' : 'gray';
})

let img = document.getElementsByClassName("img")[0];
  img.addEventListener('dblclick',function(){
    let heartImg = like.children[0];
    if(heartImg.classList.contains("text-danger")){
    
      heartImg.classList.remove("text-danger","fas");
  }
  else{
    heartImg.classList.add("text-danger","fas");
  }
});
// let img = document.getElementsByClassName("d-block w-100 img")[0];
// img.addEventListener("dblclick", function(){
//   let heartimg = like.children[0];
//   this.style.color = (this.style.color == 'gray') ? 'black' : 'gray';
// })


// let like = document.getElementsByClassName("fas fa-heart")[0];
// like.addEventListener("click", function(){
//   this.style.color = (this.style.color == 'gray') ? '#fb3958' : 'gray';
// })